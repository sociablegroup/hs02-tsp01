<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html" xmlns:fb="http://ogp.me/ns/fb#" xmlns="http://www.w3.org/1999/html">
<head>
<link href='http://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css'>
<meta charset="utf-8">
<title>Home Source Flash Sale</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache"/>
<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache"/>
<META HTTP-EQUIV="Pragma-directive" CONTENT="no-cache"/>
<META HTTP-EQUIV="Cache-Directive" CONTENT="no-cache"/>
<META HTTP-EQUIV="Expires" CONTENT="0"/>

<?php // Used to generate impression id
function generateRandomString($length = 10) {
    $characters = '0123456789eabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) { $randomString .= $characters[rand(0, strlen($characters) - 1)]; }
    return $randomString;} ?>


<!-- TEMPLATES -->
<?php include 'templates/header.php'?>
<?php include 'templates/footer.php'?>
<?php include 'templates/splash.php'?>
<?php include 'templates/policy.php'?>
<?php include 'templates/cart_to_authnet.php'?>

<?php include 'templates/main.php'?>

<?php include 'templates/products/attributes.php'?>
<?php include 'templates/products/collection_thumb.php'?>
<?php include 'templates/products/individual_thumb.php'?>
<?php include 'templates/products/lists.php'?>
<!-- END TEMPLATES -->


<!-- SCRIPTS -->
<script src="config.js?cb=12444"></script>
<script src="http://80deb2fe9e3a075ea9be-b0f5db2b124f8faed5e9e354f065bb4e.r30.cf1.rackcdn.com/allinone-v2.lib.min.js"></script>
<script src="http://80deb2fe9e3a075ea9be-b0f5db2b124f8faed5e9e354f065bb4e.r30.cf1.rackcdn.com/bootstrap-2.2.2.min.js"></script>
<script src="http://80deb2fe9e3a075ea9be-b0f5db2b124f8faed5e9e354f065bb4e.r30.cf1.rackcdn.com/allinone-v2.min.js"></script>

<script type="text/javascript">customer_data['impression_id'] = '<?php echo generateRandomString(); ?>';</script>
<!-- END SCRIPTS -->

<!-- STYLES -->
<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="/css/bootstrap-responsive.min.css"/>
<link rel="stylesheet" type="text/css" href="css/lightbox.css"/>
<link rel="stylesheet" type="text/css" href="/css/v1style.css?cb=12438"/>
<!--[if lt IE 9]>
<link rel="stylesheet" type="text/css" href="/css/ltie9.css?cb=12427"/>
<![endif]-->
<!--[if IE 7]>
<link rel="stylesheet" type="text/css" href="/css/ie7.css?cb=12427"/>
<![endif]-->
<!-- END STYLES -->


<!-- STARTUP SCRIPTS   REFACTOR THIS!! -->
<script type="text/javascript">

    var scrollLink = function(target,offset)
    {
        $('html, body').animate({
            scrollTop: $("#"+target).offset().top - offset
        }, 1000);
    };

    var addcartflash = function(){
        $('.alertAdd').css('margin-top',$(document).scrollTop() - 25);
        $('.alertAdd').stop(true,true).clearQueue().hide().css('opacity','1').show().fadeTo(4000,0,function(){
            $('.alertAdd').stop(true,true).clearQueue().hide().css('opacity','1');
        });
        return false;
    };

    var process_newsletter_signup = function(form){

        if($('#email-signup-success').css('display') != 'none') $('#email-signup-success').fadeOut();
        if($('#email-signup-failure').css('display') != 'none') $('#email-signup-failure').fadeOut();

        $('#mc_embed_signup').block({message:'<h4>Processing</h4>'});


        $.getJSON('php/list_subscribe.php','email='+$('#mce-EMAIL').val(),function(data){
            if(data.success == 1){
                $('#mc_embed_signup').unblock();
                $('#email-signup-success').fadeIn();
            }else{
                $('#mc_embed_signup').unblock();
                $('#email-signup-failure').fadeIn();
            }
        }).error(function(err){
                $('#mc_embed_signup').unblock();
                $('#email-signup-failure').fadeIn();
            });

    }

    // ROUTING
    // Kick off Knockout and Render the site
    $(document).ready(function() {
        viewModel.selectedTemplate('pages/splash');
        ko.applyBindings(viewModel);
        init_site();
        getInitialQuantities();  // Call this to get quantities for all products from the server. Only do this if all products are shown on screen at initial load.

        //setup crossroads
        crossroads.addRoute('',function(){
            viewModel.selectedTemplate(customer_data['default_template']);

        });

        crossroads.addRoute('brand/{slug}/:product_id:', function(slug,product_id){
            var brand_id = null;
            for(var i=0;i<viewModel.brands.length;i++){
                if(viewModel.brands[i].slug == slug){ brand_id = i; }
            }
            viewModel.selectedBrand(viewModel.brands[brand_id]);

            if(typeof product_id != 'undefined'){
                if(viewModel.brands[brand_id].slug == 'luxor'){
                    viewModel[viewModel.brands[brand_id]['productcollection']].filters['group'].attemptedSelect(viewModel[brands[brand_id]['productcollection']].allProducts[product_id-1].metadata['group']);
                    viewModel[viewModel.brands[brand_id]['productcollection']].filters['monogram'].attemptedSelect(viewModel[brands[brand_id]['productcollection']].allProducts[product_id-1].metadata['monogram']);
                    viewModel.selectedProduct(viewModel[viewModel.brands[brand_id]['productcollection']].selectedProduct());

                }else{
                    viewModel.selectedProduct( viewModel[viewModel.brands[brand_id]['productcollection']].allProducts[product_id-1] );
                }
                viewModel.selectedProduct( viewModel[viewModel.brands[brand_id]['productcollection']].allProducts[product_id-1] );
                viewModel.selectedTemplate('pages/product');
            }else{
                viewModel.selectedTemplate('pages/brand');
            }
        });

        crossroads.addRoute('products', function(){
            viewModel.selectedTemplate('pages/products');
        });

        crossroads.addRoute('cart', function(){
            if(viewModel.cartController.cart.quantity() == 0){
                if((valid_code || !customer_data['site_locked'])) hasher.setHash('products');
                else hasher.setHash('');
            }
            else viewModel.selectedTemplate('pages/cart');
        });

        crossroads.addRoute('policy', function(){
            viewModel.selectedTemplate('pages/policy');
        });

        crossroads.addRoute('down', function(){
            viewModel.selectedTemplate('pages/splash');
        });

        crossroads.addRoute('splash', function(){
            viewModel.selectedTemplate('pages/splash');
        });

        crossroads.addRoute('checkout', function(){
            if(viewModel.cartController.cart.quantity() == 0){
                if((valid_code || !customer_data['site_locked'])) hasher.setHash('products');
                else hasher.setHash('');
            }
            else viewModel.selectedTemplate('pages/checkout');
           // viewModel.selectedTemplate('pages/checkout');
        });

        //setup hasher
        function parseHash(newHash, oldHash){

            if(!valid_code && customer_data['site_locked'] && newHash != 'policy') {hasher.setHash(''); newHash='';}
            crossroads.parse(newHash);
        }

        hasher.initialized.add(parseHash); //parse initial hash
        hasher.changed.add(parseHash); //parse hash changes
        hasher.init(); //start listening for history change

        // Check if site is shutdown.
        var ts = Math.round((new Date()).getTime() / 1000);
        if(customer_data['end_timestamp'] <= ts && customer_data['end_timestamp'] != 0){
            hasher.setHash('down');
            crossroads.removeAllRoutes();
        }

        //if(!valid_code && customer_data['site_locked'] && newHash != 'policy') {hasher.setHash('')}

        // Setup to bind to the down (sites down) variable a trigger to show the sale ended page.
        viewModel.site.down.subscribe(function(newValue){
            if(newValue == 1){
                hasher.setHash('down');
                crossroads.removeAllRoutes();
            }
        });

        //$('.carousel').carousel({interval:2000,pause:false})
        // Set default selections for products or the first product will be selected
        //viewModel.product3Collection.filters['monogram'].attemptedSelect('A');
        //viewModel.product3Collection.filters['group'].attemptedSelect(1);
        //viewModel.collections.slice(6,7)[0].filters['size'].attemptedSelect('S');
        //viewModel.collections.slice(7,8)[0].selectedProduct(viewModel.collections.slice(7,8)[0].allProducts[1]);
    });



    // GOOGLE ANALYTICS
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-31872688-1']);
    _gaq.push(['_setDomainName', '<?php echo $_SERVER['HTTP_HOST']?>']);
    _gaq.push(['_setAllowLinker', true]);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

    // Mixpanel
    (function(d,c){var a,b,g,e;a=d.createElement("script");a.type="text/javascript";
        a.async=!0;a.src=("https:"===d.location.protocol?"https:":"http:")+
            '//api.mixpanel.com/site_media/js/api/mixpanel.2.js';b=d.getElementsByTagName("script")[0];
        b.parentNode.insertBefore(a,b);c._i=[];c.init=function(a,d,f){var b=c;
            "undefined"!==typeof f?b=c[f]=[]:f="mixpanel";g=['disable','track','track_pageview',
                'track_links','track_forms','register','register_once','unregister','identify',
                'name_tag','set_config'];
            for(e=0;e<g.length;e++)(function(a){b[a]=function(){b.push([a].concat(
                Array.prototype.slice.call(arguments,0)))}})(g[e]);c._i.push([a,d,f])};window.mixpanel=c}
        )(document,[]);
    mixpanel.init("78f219b19acfcbb3258d4121ae464b1b");


</script>
</head>

<body id="body" class="" onunload="">

<noscript>
    <div id="main_noscript">
        <div class="header"><span class="clear" />
        </div>
        <div class="content-area">
            <div>
                <p>In order to view our web site, you will need to have JavaScript enabled in your browser. To do so, please follow the instructions below:</p>
                <center><img src="/images/hr.jpg" /></center>
                <p><strong>Internet Explorer (6.0)</strong></p>
                <ol>
                    <li>Select 'Tools' from the top menu</li>
                    <li>Choose 'Internet Options'</li>
                    <li>Click on the 'Security' tab</li>
                    <li>Click on 'Custom Level'</li>
                    <li>Scroll down until you see section labled 'Scripting'</li>
                    <li>Under 'Active Scripting', select 'Enable' and click OK</li>
                </ol>
                <p><strong>Netscape Navigator (4.8)</strong></p>
                <ol>
                    <li>Select 'Edit' from the top menu</li>
                    <li>Choose 'Preferences'</li>
                    <li>Choose 'Advanced'</li>
                    <li>Choose 'Scripts &amp; Plugins'</li>
                    <li> Select the 'Enable JavaScript' checkbox and click OK</li>
                </ol>
                <p><strong>Mozilla Firefox (1.0)</strong></p>
                <ol>
                    <li>Select 'Tools' from the top menu</li>
                    <li>Choose 'Options'</li>
                    <li>Choose 'Web Features' from the left navigation</li>
                    <li>Select the checkbox next to 'Enable JavaScript' and click OK</li>
                </ol>
                <p><strong>Mozilla Firefox (1.5)</strong></p>
                <ol>
                    <li>Select 'Tools' from the top menu</li>
                    <li>Choose 'Options'</li>
                    <li>Choose 'Content' from the top navigation</li>
                    <li>Select the checkbox next to 'Enable JavaScript' and click OK</li>
                </ol>
                <p><strong>Apple Safari (1.0)</strong></p>
                <ol>
                    <li>Select 'Safari' from the top menu</li>
                    <li>Choose 'Preferences'</li>
                    <li>Choose 'Security'</li>
                    <li>Select the checkbox next to 'Enable JavaScript'</li>
                </ol>
            </div>
        </div>
        <div class="clear" style="height: 40px;">
        </div>
    </div>
</noscript>

<div id='bodymain' class="main" data-bind="template: { name: selectedTemplate(), afterRender: init }, style: {background: sitedesign['background']}"">
</div>
</body>
</html>
