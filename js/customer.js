
var Customer = function()
{
    var self = this;

    // Payment Info
    self.card_number = ko.observable();
    self.card_expiry_month = ko.observable();
    self.card_expiry_year = ko.observable();
    self.card_expiration = ko.computed(function(){
        return self.card_expiry_month()+'/'+self.card_expiry_year();
    })


    // Billing Info
    self.first_name = ko.observable();
    self.last_name = ko.observable();
    self.company = ko.observable();
    self.address1 = ko.observable();
    self.address2 = ko.observable();
    self.city = ko.observable();
    self.state = ko.observable();
    self.zipcode = ko.observable();
    self.country = ko.observable('United States');
    self.email = ko.observable();
    self.phone = ko.observable();

    // Shipping Info
    self.same_as_billing = ko.observable(true);
    self.shipping_first_name = ko.observable();
    self.shipping_last_name = ko.observable();
    self.shipping_company = ko.observable();
    self.shipping_address1 = ko.observable();
    self.shipping_address2 = ko.observable();
    self.shipping_city = ko.observable();
    self.shipping_state = ko.observable();
    self.shipping_zipcode = ko.observable();
    self.shipping_country = ko.observable();

    self.state.subscribe(function(newValue){
        if(newValue != 'Choose...'){
            $('#state-select').removeClass('error');
        }else{
            $('#state-select').addClass('error');
        }
    });

    // Internal Details
    self._same_as_bindings = ['first_name','last_name','company','address1','address2','city','state','zipcode','country'];
    self._same_as_subscriptions = [];

    self.same_as_billing.subscribe(function(same){
        if(same == true){
            self._applySameAsBindings();
        }else{
            // unregister bindings
            for(var i=0; i< self._same_as_subscriptions.length; i++){
                self._same_as_subscriptions[i].dispose();
            }
        }
    });

    self._applySameAsBindings = function(){
        // register bindings from billing to shipping values and trigger

        // First Name
        self.shipping_first_name(self.first_name());
        self._same_as_subscriptions.push(
            self.first_name.subscribe(function(newValue){
                self.shipping_first_name(newValue);
            })
        );

        // Last Name
        self.shipping_last_name(self.last_name());
        self._same_as_subscriptions.push(
            self.last_name.subscribe(function(newValue){
                self.shipping_last_name(newValue);
            })
        );

        // Company
        self.shipping_company(self.company());
        self._same_as_subscriptions.push(
            self.company.subscribe(function(newValue){
                self.shipping_company(newValue);
            })
        );

        // Address1
        self.shipping_address1(self.address1());
        self._same_as_subscriptions.push(
            self.address1.subscribe(function(newValue){
                self.shipping_address1(newValue);
            })
        );

        // Address2
        self.shipping_address2(self.address2());
        self._same_as_subscriptions.push(
            self.address2.subscribe(function(newValue){
                self.shipping_address2(newValue);
            })
        );

        // City
        self.shipping_city(self.city());
        self._same_as_subscriptions.push(
            self.city.subscribe(function(newValue){
                self.shipping_city(newValue);
            })
        );

        // State
        self.shipping_state(self.state());
        self._same_as_subscriptions.push(
            self.state.subscribe(function(newValue){
                self.shipping_state(newValue);
            })
        );
        // Zipcode
        self.shipping_zipcode(self.zipcode());
        self._same_as_subscriptions.push(
            self.zipcode.subscribe(function(newValue){
                self.shipping_zipcode(newValue);
            })
        );
        // Country
        self.shipping_country(self.country());
        self._same_as_subscriptions.push(
            self.country.subscribe(function(newValue){
                self.shipping_country(newValue);
            })
        );

    };

    self.get_state_abbr = function(state){
        return stateMappings[state];
    }

    self.validate = function(){
        jQuery.validator.addMethod("phoneUS", function(phone_number, element) {
            phone_number = phone_number.replace(/\s+/g, "");
            return this.optional(element) || phone_number.length > 9 &&
                phone_number.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
        }, "Please specify a valid phone number");

        $("#checkout-form").validate({
            rules: {
                card_number: {
                    required: true,
                    creditcard: true
                },
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                address1: {
                    required: true
                },
                city: {
                    required: true
                },
                zipcode:{
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                phone: {
                    required: true
                },
                shipping_first_name: {
                    required: true
                },
                shipping_last_name: {
                    required: true
                },
                shipping_address1: {
                    required: true
                },
                shipping_city: {
                    required: true
                },
                shipping_zipcode:{
                    required: true
                }
            }
        });

        $('#checkout-form').submit();
    }

    if(self.same_as_billing()) self._applySameAsBindings();
}