<script id="header" type="text/html">
    <img src="http://ad.retargeter.com/seg?add=481387&t=2" width="1" height="1" style="position:absolute;visibility:hidden" />

    <div class="navbar navbar-fixed-top" data-bind="style: {background: sitedesign['headerbg']}">
        <div class="container" style="padding-left:0px;padding-right:0px;padding-bottom:7px;">

            <div class="row-fluid">
                <div class="span5">
                    <img id="logo" src="images/specific/logo.png?cb=1001" style="margin-top:12px"/>
                    <img src="images/specific/hslogo.png" style="margin-top:13px;margin-left:25px"/>
                </div>
                <div class="span4">
                    <img data-bind="visible: false" id="jsdlogo" src="images/jilllogo.png" style="width:137px;margin-top:10px;float:right"/>
                    <img data-bind="visible: appsettings['partner'] == 'GMA'" id="gmalogo" src="images/gma-white.jpg" style="width:166px;margin-top:10px;float:right"/>
                </div>
                <div class="span3">
                    <div id="shopping-cart-btn" style="float:right; font-weight:bold;font-size:98%;margin-top:30px;padding:1px;" data-bind="style: {background: (selectedTemplate()=='pages/cart') ? 'transparent' : 'transparent'}">
                        <a data-bind="click: function(){ 	if ( cartController.cart.items().length > 0) { hasher.setHash('cart'); }
                                                                else { alert('No items in cart'); } },
                                                visible: !(selectedTemplate() == 'pages/cart' || selectedTemplate() == 'pages/splash')" class="btn btn-large pull-right" href="#">Shopping Cart (<span data-bind="text: cartController.cart.quantity() + ' item'"></span><span data-bind="text: cartController.cart.quantity() == 1 ? '' : 's'"></span>)</a>
                        <span style="margin-top:10px; color: #6F6D68;font-size: 85%;background: transparent" data-bind="visible: selectedTemplate() == 'pages/cart'" class="pull-right" href="#">Viewing Cart</span>
                    </div>
                </div>
            </div>

        </div>
    </div>
</script>