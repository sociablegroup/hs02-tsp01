<script id="footer" type="text/html">

    <div class="container footer">
        <div class="row" style="margin-top:16px;">
            <div class="span6">
                <a style="display:none" href="https://www.facebook.com/AlisaMichelleDesigns"><img src="images/facebookflaticon.png" style="margin-bottom: 8px; width: 23px;"/></a>
                <div class="red">All sales are final</div>
                <div><a class="accent" style="cursor:pointer;" data-bind="click: function(){hasher.setHash('policy')}">Shipping & Return Policy</a></div>
                <div class="accent">contact: <a class="accent"  data-bind="attr:{href:'mailto:'+customer_data['support_email']}, text:customer_data['support_email']"></a></div>
                <div class="credits" data-bind="text: 'Copyright 2012 '+customer_data['company_name']+'&trade;. All rights reserved.'"></div>
            </div>
            <div class="span6" style="text-align:right">

                <a class="credits" href="http://www.sociableshops.com">Powered by SociableShops</a>
            </div>
        </div>
    </div>

    <!--
            <div class="footer">
                    <div class="container">
                        <div class="row" style="padding-top:8px;">
                            <div class="span6" >
                                <a onclick="hasher.setHash('policy')" style="color:red;font-weight:bold">Shipping & Returns</a><br/>
                                Copyright © 2012 Ariel Gordon Jewelry. All Rights Reserved<br/>
                                Support: <a data-bind="attr: {href: 'mailto:'+customer_data['support_email']}"><span data-bind="text: customer_data['support_email']"></span></a>
                            </div>

                            <div class="">
                               <a class="pull-right" href="http://www.sociableshops.com">Powered by SociableShops</a>
                            </div>
                        </div>
                    </div>
                </div>
    -->



    <!-- popin -->
    <div id="added-to-cart-notification" class="alert alert-info alertAdd">
        <h2>Product added to your cart!</h2>
    </div>
</script>


<script id="splashfooter" type="text/html">
    <div class="footer" style="background:transparent">
        <div class="container">
            <div class="row" style="margin-right:57px;">
                <div class="span6" style="margin-left:77px;">
                    <!-- <a onclick="hasher.setHash('policy')" style="color:red;font-weight:bold">Shipping & Returns</a><br/> -->
                    Copyright © 2012 Bella Pierre. All Rights Reserved <br/>
                    Support: <a data-bind="attr: {href: 'mailto:'+customer_data['support_email']}"><span data-bind="text: customer_data['support_email']"></span></a>
                </div>

                <div class="">
                    <a class="pull-right" href="http://www.sociableshops.com">Powered by SociableShops</a>
                </div>
            </div>
        </div>
    </div>
</script>