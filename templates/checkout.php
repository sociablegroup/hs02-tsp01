<script id="pages/checkout" type="text/html">
    <div data-bind="template: { name: 'header'}">
    </div>

    <div class="container cart-page checkout-page">
        <div class="page-wrapper" style="padding:20px">

            <div class="row-fluid header-row">
                <div class="span12">
                    <h1> Checkout</h1>
                </div>
            </div>



            <div class="row-fluid">
                <div class="span5">
                    <form id="checkout-form" class="form">
                        <div class="row-fluid">
                            <div class="span12">
                                <h3>Payment Information</h3>
                                <div class="row-fluid" style="margin-bottom:8px">
                                    <img class="cc-icon" src="images/visa.gif"/>
                                    <img class="cc-icon" src="images/mastercard.gif"/>
                                    <img class="cc-icon" src="images/discover.gif"/>
                                    <img class="cc-icon" src="images/amex.gif"/>
                                </div>
                                <div class="row-fluid">
                                    <div class="span6">

                                        <label>*Card Number</label>
                                        <input name="card_number" id="card_number" data-bind="value: cartController.customer.card_number, valueUpdate: 'afterkeydown'"type="text" class="input-block-level"/>
                                    </div>
                                    <div class="span6">
                                        <label>*Expiration Date</label>
                                        <select data-bind="value: cartController.customer.card_expiry_month" class="input-mini">
                                            <option value="01">Jan</option>
                                            <option value="02">Feb</option>
                                            <option value="03">Mar</option>
                                            <option value="04">Apr</option>
                                            <option value="05">May</option>
                                            <option value="06">Jun</option>
                                            <option value="07">Jul</option>
                                            <option value="08">Aug</option>
                                            <option value="09">Sep</option>
                                            <option value="10">Oct</option>
                                            <option value="11">Nov</option>
                                            <option value="12">Dec</option>
                                        </select>
                                        <select data-bind="value: cartController.customer.card_expiry_year" class="input-small">
                                            <option value="13">2013</option>
                                            <option value="14">2014</option>
                                            <option value="15">2015</option>
                                            <option value="16">2016</option>
                                            <option value="17">2017</option>
                                            <option value="18">2018</option>
                                            <option value="19">2019</option>
                                            <option value="20">2020</option>
                                            <option value="21">2021</option>
                                            <option value="22">2022</option>
                                            <option value="23">2023</option>
                                            <option value="24">2024</option>
                                            <option value="25">2025</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span12">
                                <h3>Billing Information</h3>
                                <div class="row-fluid">
                                    <div class="span6">
                                        <label>*First Name</label>
                                        <input name="first_name" id="first_name" data-bind="value: cartController.customer.first_name, valueUpdate: 'afterkeydown'" class="input-block-level" type="text">
                                    </div>
                                    <div class="span6">
                                        <label>*Last Name</label>
                                        <input name="last_name" id="last_name" data-bind="value: cartController.customer.last_name, valueUpdate: 'afterkeydown'" class="input-block-level" type="text">
                                    </div>
                                </div>

                                <label>Company Name</label>
                                <input data-bind="value: cartController.customer.company, valueUpdate: 'afterkeydown'" type="text">
                                <label>*Address 1</label>
                                <input name="address1" id="address1" data-bind="value: cartController.customer.address1, valueUpdate: 'afterkeydown'" type="text" class="input-block-level">
                                <label>Address 2</label>
                                <input name="address2" data-bind="value: cartController.customer.address2, valueUpdate: 'afterkeydown'" type="text" class="input-block-level">

                                <div class="row-fluid">
                                    <div class="span5">
                                        <label>*City</label>
                                        <input name="city" id="city" data-bind="value: cartController.customer.city, valueUpdate: 'afterkeydown'" type="text" class="input-block-level">
                                    </div>
                                    <div class="span3">
                                        <lable>*State</lable>
                                        <select class="input-block-level" data-bind="options: cartController.shippingRule.availableStates,  value: cartController.customer.state, optionsCaption: 'Select state'">
                                        </select>
                                    </div>
                                    <div class="span4">
                                        <label>*Zip/Postal Code</label>
                                        <input name="zipcode" id="zipcode" data-bind="value: cartController.customer.zipcode, valueUpdate: 'afterkeydown'" type="text" class="input-block-level">
                                    </div>
                                </div>

                                <div class="row-fluid">
                                    <div class="span6">
                                        <label>*Email</label>
                                        <input name="email" id="email" data-bind="value: cartController.customer.email, valueUpdate: 'afterkeydown'" class="input-block-level" type="text">
                                    </div>
                                    <div class="span6">
                                        <label>*Phone</label>
                                        <input name="phone" id="phone" data-bind="value: cartController.customer.phone, valueUpdate: 'afterkeydown'" class="input-block-level" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span12">
                                <h3>Shipping Information</h3>
                                <div style="background:#efefef; padding:5px;margin-bottom:10px">
                                    <label class="checkbox">
                                        <input type="checkbox" value="" data-bind="checked: cartController.customer.same_as_billing">
                                        Same as Billing Information
                                    </label>
                                </div>

                                <div data-bind="visible: (cartController.customer.same_as_billing() != true)">
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <label>*First Name</label>
                                            <input name="shipping_first_name" id="shipping_first_name" data-bind="value: cartController.customer.shipping_first_name, valueUpdate: 'afterkeydown'" class="input-block-level" type="text">
                                        </div>
                                        <div class="span6">
                                            <label>*Last Name</label>
                                            <input name="shipping_last_name" id="shipping_last_name" data-bind="value: cartController.customer.shipping_last_name, valueUpdate: 'afterkeydown'" class="input-block-level" type="text">
                                        </div>
                                    </div>

                                    <label>Company Name</label>
                                    <input name="shipping_company" data-bind="value: cartController.customer.shipping_company, valueUpdate: 'afterkeydown'" type="text">
                                    <label>*Address 1</label>
                                    <input name="shipping_address1" id="shipping_address1" data-bind="value: cartController.customer.shipping_address1, valueUpdate: 'afterkeydown'" type="text" class="input-block-level">
                                    <label>Address 2</label>
                                    <input name="shipping_address2" data-bind="value: cartController.customer.shipping_address2, valueUpdate: 'afterkeydown'" type="text" class="input-block-level">

                                    <div class="row-fluid">
                                        <div class="span5">
                                            <label>*City</label>
                                            <input name="shipping_city" id="shipping_city" data-bind="value: cartController.customer.shipping_city, valueUpdate: 'afterkeydown'" type="text" class="input-block-level">
                                        </div>
                                        <div class="span3">
                                            <lable>*State</lable>
                                            <select name="shipping_state" class="input-block-level" data-bind="options: cartController.shippingRule.availableStates, value: cartController.customer.shipping_state, optionsCaption: 'Select state'">
                                            </select>
                                        </div>
                                        <div class="span4">
                                            <label>*Zip/Postal Code</label>
                                            <input name="shipping_zipcode" id="shipping_zipcode" data-bind="value: cartController.customer.shipping_zipcode, valueUpdate: 'afterkeydown'" type="text" class="input-block-level">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="span7" style="background:#efefef;border:1px solid #ddd;padding:10px;">
                    <div class="row-fluid">
                        <div class="span12">
                            <h3 style="margin-top:0px;">Review your order</h3>
                            <div class="row-fluid" style="font-size:12px;">
                                <div class="span3">
                                    <strong>Payment information</strong>
                                    <div data-bind="text: 'Card Number: '+cartController.customer.card_number(), visible: cartController.customer.card_number() != undefined"></div>
                                    <div data-bind="text: 'Expiration: '+cartController.customer.card_expiration()" style="margin-bottom:5px"></div>
                                    <strong>Shipping address</strong>
                                    <div>
                                        <span data-bind="text: cartController.customer.shipping_first_name()+' ', visible: cartController.customer.shipping_first_name() != undefined"></span>
                                        <span data-bind="text: cartController.customer.shipping_last_name(), visible: cartController.customer.shipping_last_name() != undefined"></span>
                                    </div>
                                    <div data-bind="text: cartController.customer.shipping_company(), visible: cartController.customer.shipping_company() != undefined"></div>
                                    <div data-bind="text: cartController.customer.shipping_address1(), visible: cartController.customer.shipping_address1() != undefined"></div>
                                    <div data-bind="text: cartController.customer.shipping_address2(), visible: cartController.customer.shipping_address2() != undefined"></div>
                                    <div>
                                        <span data-bind="text: cartController.customer.shipping_city(), visible: cartController.customer.shipping_city() != undefined"></span><span data-bind="text: ', '+cartController.customer.get_state_abbr(cartController.customer.shipping_state()), visible: cartController.customer.shipping_state() != undefined"></span>
                                        <span data-bind="text: '  '+cartController.customer.shipping_zipcode(), visible: cartController.customer.shipping_zipcode() != undefined"></span>
                                    </div>
                                    <div data-bind="text: cartController.customer.shipping_country(), visible: cartController.customer.shipping_country() != undefined"></div>
                                </div>
                                <div class="span4">
                                    <strong>Billing address</strong>
                                    <div>
                                        <span data-bind="text: cartController.customer.first_name()+' ', visible: cartController.customer.first_name() != undefined"></span>
                                        <span data-bind="text: cartController.customer.last_name(), visible: cartController.customer.last_name() != undefined"></span>
                                    </div>
                                    <div data-bind="text: cartController.customer.company(), visible: cartController.customer.company() != undefined"></div>
                                    <div data-bind="text: cartController.customer.address1(), visible: cartController.customer.address1() != undefined"></div>
                                    <div data-bind="text: cartController.customer.address2(), visible: cartController.customer.address2() != undefined"></div>
                                    <div>
                                        <span data-bind="text: cartController.customer.city(), visible: cartController.customer.city() != undefined"></span><span data-bind="text: ', '+cartController.customer.get_state_abbr(cartController.customer.state()), visible: cartController.customer.state() != undefined"></span>
                                        <span data-bind="text: '  '+cartController.customer.zipcode(), visible: cartController.customer.zipcode() != undefined"></span>
                                    </div>
                                    <div data-bind="text: cartController.customer.country(), visible: cartController.customer.country() != undefined"></div>
                                    <div data-bind="text: 'Phone: '+cartController.customer.phone(), visible: cartController.customer.phone() != undefined"></div>
                                    <div data-bind="text: cartController.customer.email(), visible: cartController.customer.email() != undefined"></div>
                                </div>
                                <div class="span5" style="background:#ddd;border-radius:7px;text-align:center;padding:8px;">
                                    <div id="orderbutton" class="btn btn-success" data-bind="click: cartController.customer.validate">Place your order</div>
                                    <div style="background:#fff;border-radius:7px;padding:5px;margin-top:10px;">
                                        <strong style="margin-top:0px">Order Summary</strong>
                                        <table id="cart-summary" style="text-align:left;margin:0px auto">
                                            <tr>
                                                <td>Items</td>
                                                <td class="cart-value" data-bind="text: formatCurrency(viewModel.cartController.subTotal())"></td>
                                            </tr>
                                            <tr>
                                                <td>Shipping & Handling <i style="display:none" class="icon-info-sign" rel="tooltip" title="Shipping costs: $4.95/order"></i></td>
                                                <td class="cart-value" data-bind="text: formatCurrency(viewModel.cartController.shippingTotal())"></td>
                                            </tr>
                                            <tr style="border-bottom:1px solid #aaa">
                                                <td>Tax</td>
                                                <td class="cart-value" data-bind="text: formatCurrency(viewModel.cartController.taxTotal())"></td>
                                            </tr>
                                            <tr>
                                                <td><h2>Order Total</h2></td>
                                                <td class="cart-value"><h2 data-bind="text: formatCurrency(viewModel.cartController.grandTotal())"></h2></td>
                                            </tr>
                                        </table>
                                    </div>

                                </div>
                            </div>


                            <h4>Order contents</h4>
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <th style="text-align:left">Name</th>
                                    <th style="text-align:left">Description</th>
                                    <th style="text-align:center">Quantity</th>
                                    <th>Unit Price</th>
                                    <th>Total Price</th>
                                </tr>
                                </thead>
                                <tbody data-bind='foreach: viewModel.cartController.cart.items()'>
                                <tr class="">
                                    <td style="text-align:left" data-bind="text: product.name"></td>
                                    <td style="text-align:left" data-bind="text:  product.metadata_string()"></td>
                                    <td style="text-align:center" data-bind="text: attemptedQuantity"></td>
                                    <td data-bind="text:formatCurrency(product.price())"></td>
                                    <td data-bind="text:formatCurrency(subtotal())"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>





        </div>
    </div>


    <div class="footer" data-bind="template: { name: 'footer'}">
    </div>
</script>
