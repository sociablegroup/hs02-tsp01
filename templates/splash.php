<script id="pages/splash" type="text/html">


    <div class="container splash-wall-container">
        <div class="row">
            <div class="splash-entry-code">
                <div class="" data-bind="visible: !(customer_data['sale_ended'] || site.down()==1)">
                    <p style="font-size:18px;" data-bind="visible: appsettings['partner'] == 'JSD'">Enter the promo code found on the Today Show website to get your deal!</p>
                    <p style="font-size:18px;" data-bind="visible: appsettings['partner'] == 'GMA'">Enter the promo code found on the GMA website to get your deal!</p>
                    <p style="font-size:18px;" data-bind="visible: !(appsettings['partner'] == 'GMA' || appsettings['partner'] == 'JSD')">Enter the promo code to get your deal!</p>

                    <form data-bind="submit:function(){ if(check_code()){ hasher.setHash('products')}else{ alert('Invalid code. Please try again'); } }">
                        <div class="controls">

                                <input class="span3"  id="input_promocode" size="16" type="text">
                            <button class="btn btn-large btn-gay-orange" id="promo_submit" type="submit">submit</button>

                        </div>
                    </form>
                </div>
                <div class="sale-ended-container" data-bind="visible: (customer_data['sale_ended'] || site.down()==1)">
                    <p data-bind="html: site.down_message()"></p>
                    <p>YOU CAN CONTACT US AT</p>
                    <a data-bind="attr:{href:'mailto:'+customer_data['support_email']}, text: customer_data['support_email']"></a>
                </div>
            </div>

        </div>
    </div>

    <!-- scroller

    <div class="container" style="margin-top:5px">

        <div class="row" style="margin-bottom:5px;">
            <div class="span6">
                <img style="width:200px" src="images/specific/logo.png"/>
            </div>
            <div class="span6">
                <img class="pull-right" style="width:139px;margin-top:15px;" src="images/jilllogo.png"/>
            </div>
        </div>
        <div id="myCarousel" class="carousel slide">
            <div class="carousel-inner">
                <div class="item active">
                    <img src="images/specific/scroller1.jpg" alt="">
                </div>
                <div class="item">
                    <img src="images/specific/scroller2.jpg" alt="">
                </div>
                <div class="item">
                    <img src="images/specific/scroller3.jpg" alt="">
                </div>
                <div class="item">
                    <img src="images/specific/scroller4.jpg" alt="">
                </div>
                <div class="item">
                    <img src="images/specific/scroller5.jpg" alt="">
                </div>
                <div class="item">
                    <img src="images/specific/scroller6.jpg" alt="">
                </div>
                <div class="item">
                    <img src="images/specific/scroller7.jpg" alt="">
                </div>
                <div class="item">
                    <img src="images/specific/scroller8.jpg" alt="">
                </div>
                <div class="item">
                    <img src="images/specific/scroller9.jpg" alt="">
                </div>
                <div class="item">
                    <img src="images/specific/scroller10.jpg" alt="">
                </div>
            </div>
            <div class="carousel-caption">
                <div class="row">
                    <div class="span7">
                        <h4>Enter the promo code found on the Today Show website to get this deal!</h4>
                    </div>
                    <div class="span4">
                        <form style="margin-bottom:0px" data-bind="submit:function(){ if(check_code()){ hasher.setHash('products')}else{ alert('Invalid code. Please try again'); } }">
                            <div class="controls">
                                <div class="input-append">
                                    <input class="span3"  id="input_promocode" size="16" type="text">
                                    <button class="btn btn-large btn-gay-orange"
                                            id="promo_submit" type="submit">submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">‹</a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">›</a>
        </div>


    </div>
    -->
    <div style="display:none" data-bind="template: {name: 'splashfooter'}">
    </div>
</script>