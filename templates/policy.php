<script id="pages/policy" type="text/html">
    <div  data-bind="template: { name: 'header'}">
    </div>
    <div class="container" style="">
        <div class="page-wrapper shipping-page" style="padding:20px;padding-bottom:150px">
            <div class="page-layout" style="padding:25px">
                <h1 style="text-align:left">Shipping & Return Policy</h1>
                <div class='btn' style="margin-top:8px;margin-bottom:12px" data-bind="click:function(){ hasher.setHash('products');}"><i class="icon-chevron-left"></i> CONTINUE SHOPPING</div>
                <ul style="margin-top:35px">
                    <li>Order confirmation emails are sent to the address submitted at the time of your order which include your order identification number.  This number can be used to reference your order.</li>
                    <li data-bind="text: 'Please contact us at: '+customer_data['support_email']+' or 310.456.1582 with any change request to your order.  We will make every effort to fulfill your order change request, however, we make no guarantee of our ability to accommodate the request.'"></li>
                    <li>If you emailed us with an address change, we will do our best to accommodate you.</li>
                    <li data-bind="text:'Please note that due to high demand '+customer_data['order_offer']"></li>
                    <li>ALL SALES ARE FINAL SALES. NO RETURNS NO EXCHANGES.</li>
                </ul>
            </div>
        </div>
    </div>
    <div data-bind="template: { name: 'footer'}">
    </div>
</script>