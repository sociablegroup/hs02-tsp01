<script id="pages/cart" type="text/html">
    <div data-bind="template: { name: 'header'}">
    </div>

    <div class="container cart-page">
        <div class="page-wrapper page-layout" style="padding:20px">
            <div class="row-fluid header-row">
                <div class="span4">
                    <h1>Shopping Cart</h1>
                </div>
                <div class="span4">
                    &nbsp;
                </div>
                <div class="span4">
                    <button style="margin-top:11px" class="pull-right btn btn-warning" onclick="alert('Purchasing is disabled.')"
                            data-bind="visible: typeof viewModel.cartController.customer.state() !== 'undefined'"
                        >CHECK OUT NOW <i class="icon-chevron-right icon-white"></i></button>
                </div>
            </div>

            <div class="cart product-list-container" data-bind='template: {name: "shopping/cart_row2", foreach: cartController.cart.items() }'>
            </div>

            <div class="row-fluid">
                <div class="span12">
                    <div data-bind="css: {alert:typeof cartController.customer.state() === 'undefined'}" style="padding: 12px 11px 12px 14px;margin-bottom:10px;height:20px;">
                        <span data-bind="visible: typeof cartController.customer.state() === 'undefined'"><strong>Action Required!</strong> You must select your billing state before you can proceed to checkout.</span>
                        <select class="select-state-combo" data-bind="options: cartController.shippingRule.availableStates, value: cartController.customer.state, optionsCaption: 'Select billing state to checkout'">
                        </select>
                    </div>
                </div>
            </div>

            <div class="row-fluid" style="margin-top:0px">
                <div style="float:right;overflow:hidden">
                    <table id="cart-summary" class="span4" style="width:205px">
                        <tr>
                            <td>Subtotal</td>
                            <td class="cart-value" data-bind="text: formatCurrency(viewModel.cartController.subTotal())"></td>
                        </tr>
                        <tr>
                            <td>Shipping & Handling <i style="display:none" class="icon-info-sign" rel="tooltip" title="Shipping costs: $4.95/order"></i></td>
                            <td class="cart-value" data-bind="text: formatCurrency(viewModel.cartController.shippingTotal())"></td>
                        </tr>
                        <tr style="border-bottom:1px solid #aaa">
                            <td>Tax</td>
                            <td class="cart-value" data-bind="text: formatCurrency(viewModel.cartController.taxTotal())"></td>
                        </tr>
                        <tr>
                            <td><h2>Order Total</h2></td>
                            <td class="cart-value"><h2 data-bind="text: formatCurrency(viewModel.cartController.grandTotal())"></h2></td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="row-fluid footer-row">
                <div class="span6">
                    <div class="btn" data-bind="click: function(){hasher.setHash('products');}"><i class="icon-chevron-left"></i> CONTINUE SHOPPING</div>
                </div>
                <div class="span6">
                    <button class="pull-right btn btn-warning" data-bind="enable: typeof viewModel.cartController.customer.state() !== 'undefined'"
                            onclick="alert('Purchasing is disabled.')">
                        CHECK OUT NOW <i class="icon-chevron-right icon-white"></i></button>
                </div>
            </div>

        </div>
    </div>

    <div data-bind="template: { name: 'footer'}">
    </div>


</script>

<!-- --------------------------  CART ROW ------------------- -->
<script id="shopping/cart_row2" type="text/html">
    <div class="row-fluid item-row">
        <div class="span2 image-container">
            <img data-bind="attr: { src: 'mini/'+product.media[0], title: product.name }"/>
        </div>
        <div class="span10">
            <div class="row-fluid main-info">
                <div class="span6">
                    <h2 class="product-name" data-bind="html: product.name"><small class="metadata" data-bind="html: product.metadata_string(), visible: product.has_metadata()"></small></h2>
                    <h3 style="text-transform:none" data-bind="html: viewModel.brands[product.brand_id].name"></h3>
                    <p data-bind="html: product.metadata_string()"></p>
                </div>
                <div class="span1" data-bind="html: formatCurrency(product.price())">
                </div>
                <div class="span1">
                    <input name="qty" style="font-size:14px" type="text" class="span8 quantity" onchange="" data-bind='value: attemptedQuantity, valueUpdate: "afterkeydown"' size="2" />
                </div>
                <a style="cursor:pointer" class="span1" data-bind='click: function() { viewModel.cartController.cart.removeItem($data) }'>
                    Remove
                </a>
                <div class="span3">
                    <h2 class="pull-right" data-bind='html: formatCurrency(subtotal())'></h2>
                </div>
            </div>
            <div class="row-fluid secondary-info">
                <div class="span12">
                    <div><strong>Ship Date:</strong> <span data-bind="text: customer_data['ship_date_range']"></span></div>
                    <div><strong>Return Policy:</strong> This item is final sale and non-returnable.</div>
                </div>
            </div>
        </div>
        <div class="separator"></div>
    </div>
</script>