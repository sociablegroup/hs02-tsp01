<script id="pages/products" type="text/html">

    <div data-bind="template: { name: 'header'}">
    </div>

    <div class="container products-page page-wrapper">

        <div class="page-layout">
            <div class="row-fluid">
                <div class="span12">

                    <div class="row-fluid" data-bind="visible: viewModel.site.show_on_hold_banner() == 1">
                        <div class="span10 offset1">
                            <div class="alert" style="font-size:16px;padding:25px;margin-top:30px">
                                <strong data-bind="text: site.banner_title()"></strong>
                                <span data-bind="html: site.banner_message()"></span>
                            </div>
                        </div>
                    </div>


                    <div class="section">

                        <div class="row-fluid">
                            <div class="span12">
                                <h1 class="section-header accent" style="margin-top:20px" data-bind="html: viewModel.collections[0].allProducts[0].name+'<br/><small>By Home Source International</small>'"></h1>
                            </div>
                        </div>
                        <div class="row-fluid" style="margin-top:15px;margin-bottom:15px">
                            <div class="span3" style="text-align:right">
                                <img src="images/specific/topshot.jpg"/>
                            </div>
                            <div class="span9" style="margin-top:20px">
                                <p class="section-description" data-bind="html: viewModel.collections[0].allProducts[0].description"></p>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span12">
                                <ul class="thumbnails" data-bind="template:{ name: 'product/thumb1', foreach: viewModel.collections[0].allProducts}">
                                </ul>
                            </div>
                        </div>

                    </div>



                    <div class="row-fluid" style="margin-bottom:20px;">
                        <div class="span10 offset1">
                            <h1 class="section-header" style="text-align:left;font-size:22px">WASHING INSTRUCTIONS</h1>
                            <p>To insure the natural softness, it is recommended that your towels be routinely washed with one-third the amount of detergent recommended by the detergent manufacturer and never washed or dried with fabric softener.
                                Tumble dry on low (preferred) to medium heat - Dry no more than a half load of towels at a time to keep them fluffy.</p>
                        </div>
                    </div>


                    <div class="row-fluid bio" style="margin-top:35px;margin-bottom:55px;">
                        <div class="span2 offset1" style="text-align:center">
                            <img src="../images/specific/bio.jpg"/>
                        </div>
                        <div class="span8" style="margin-top:15px;">
                            <h1 class="section-header" style="text-align:left;font-size:22px;text-transform:uppercase" data-bind="html: 'About '+customer_data['company_name']"></h1>
                            <p>Home Source International is a leader in the Home Fashions Industry with Unique, and Innovative Products, and constantly pushes the envelope with pioneering Product Development, Compelling Color Stories, and Sustainable and Environmentally-Conscious Fibers. MicroCotton® Towels were rated the Best Towels in the world by Real Simple magazine and has been Home Source International’s Best-Selling Towel for more than a decade.</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div data-bind="template: { name: 'footer'}">
    </div>
</script>