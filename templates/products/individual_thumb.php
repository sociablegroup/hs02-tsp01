<script id="product/thumb1" type="text/html">
    <li class='product-thumb1'>
        <div class="thumbnail">
            <a class="lightbox" rel="lightbox" data-bind="attr: { href: '/zoom/'+media[0] }" >
                <img class="thumb" data-bind="attr: { src: '/thumb/'+media[0] }"/>
                <div class="zoomIcon">click</div>
            </a>
        </div>

        <div class="under-thumbnail">
            <div class="product-title accent" data-bind="html: name +' - '+ metadata['color']"></div>

            <div class="row-fluid">
                <div class="span9">
                    <div class="regular-price" data-bind="text: 'Retail: '+formatCurrency(retail_price)"></div>
                    <div class="sale-price" data-bind="html: 'Steal: '+formatCurrency(sale_price)+' <br/>('+percentOff()+'% off)'"></div>
                </div>
                <div class="span3" style="text-align:right">
                    <label>Quantity</label>
                    <input data-bind="attr: {id: 'quantity'+id()}" class="quantity" value="1" type="text" size="3" style="width:35px;margin-bottom:3px;"/>
                </div>
            </div>
            <div class="row-fluid" style="margin-top:6px">

                <!-- One Checkout Button -->
                <div class="span12" style="text-align:center">
                    <div class="btn btn-warning"
                         data-bind="click: function() {
                                viewModel.cartController.cart.addItem($data, $('#quantity'+id()).val()); },
                                visible:quantity() > 0 && on_hold() != 1 ">
                        Add to Cart <i style="display:none;" class="icon-chevron-right icon-white"></i>
                    </div>
                    <div class="btn btn-inverse btn-mini onhold-mini disabled"
                         style="margin-top:15px;margin-bottom:14px"
                         data-bind="visible: on_hold() == 1 || quantity() <= 0">
                        On Hold<br/><small>Come back soon</small>
                    </div>

                </div>

                <!-- Checkout and more info Buttons -->
                <div class="row-fluid" style="margin-top:10px;display:none">
                    <div class="span6">
                        <a data-bind="attr: { href: '#myModal'+id()}" role="button" class="btn" data-toggle="modal">More Info</a>
                    </div>
                    <div class="span6" style="text-align:right">
                        <div class="btn btn-warning"
                             data-bind="click: function() {
                                viewModel.cartController.cart.addItem($data, $('#quantity'+id()).val()); },
                                visible:quantity() > 0 && on_hold() != 1 ">
                            Add to Cart <i style="display:none;" class="icon-chevron-right icon-white"></i>
                        </div>
                        <div class="btn btn-inverse btn-mini onhold-mini disabled"
                             style="margin-top:15px;margin-bottom:14px"
                             data-bind="visible: on_hold() == 1 || quantity() <= 0 ">
                            On Hold<br/><small>Come back soon</small>
                        </div>

                    </div>
                </div>


            </div>
        </div>
        <!-- Modal -->
        <div data-bind="attr: {id:'myModal'+id()}" class="modal hide fade" style="text-align:left" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Product Information</h3>
            </div>
            <div class="modal-body">
                <h4 data-bind="html: name"></h4>
                <div data-bind="html: description"></div>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            </div>
        </div>
    </li>
    <!-- <div style="clear:both" data-bind="visible: ($index()+1)%4 == 0"></div> -->


</script>