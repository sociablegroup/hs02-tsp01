<script id="attribute/none" type="text/html">

</script>

<script id="attribute/default" type="text/html">
    <!-- ko foreach: $data.get_filters_array() -->
    <div class="attribute-container">
        <label data-bind="text: name"></label>
        <select data-bind="options: $parent.getAvailableOptions(name,true),
                            value: $parent.filters[name].attemptedSelect,
                            event: {change: function(){}  }" style="width:113px;">
        </select>
        <div data-bind="if: $parent.selectedProduct().sizechart">
            <div data-bind="if: (name == 'size')">
                <a class="lightbox" rel="lightbox" data-bind="attr: { href: 'images/'+$parent.selectedProduct().sizechart }" >
                    (size chart)
                </a>
            </div>
        </div>
    </div>
    <!-- /ko -->
</script>

<script id="attribute/monogram" type="text/html">
    <div class="pull-right quantity" style="margin-bottom:8px">
        <label>Monogram <i class="icon-info-sign" rel="tooltip" title="Only one monogram per order per product. If you update the monogram and add more products it will update the monogram in your cart."></i></label>
        <input type="text" size="3" data-bind="event: {keyup:  function(){ $data.metadata['monogram'] = $('#monogram').val();}}" class="" id="monogram" style="width:45px">
    </div>
</script>

<script id="attribute/monogram-single-letter" type="text/html">
    <div class="pull-right quantity" style="margin-bottom:8px">
        <select data-bind="options: $root[$root.brands[$data.brand_id]['productcollection']].filters['monogram'].options,
            value: $root[$root.brands[$data.brand_id]['productcollection']].filters['monogram'].attemptedSelect,
            event: {change: function(){
                     $root[$root.brands[$data.brand_id]['productcollection']].filters['group'].attemptedSelect($root.selectedProduct().metadata['group']);
                     $root.selectedProduct($root[$root.brands[$data.brand_id]['productcollection']].selectedProduct());
                     //hasher.setHash('brand/'+$root.brands[$data.brand_id].slug+'/'+$root.selectedProduct()._id);
                     $('.zoom-enabled').zoom({url: $('.zoom-enabled img').attr('rel'), on: 'click'});
                }
            }" class="pull-right" style="width:55px">

        </select>
        <label class="pull-right" style="line-height:2;margin-right:3px;">Monogram</label>
    </div>
</script>