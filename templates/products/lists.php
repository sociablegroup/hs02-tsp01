<script id="products/none" type="text/html">
    <div>No products available for this brand.</div>
</script>

<script id="products/list" type="text/html">
    <ul class="thumbnails" data-bind="template: {name: 'product/thumb1', foreach: $data.allProducts}">
    </ul>
</script>
