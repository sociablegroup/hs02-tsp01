<script id="product/grouped_thumb" type="text/html">
    <li class='product-thumb1' data-bind="with: $data.selectedProduct">
        <div class="thumbnail">
            <a class="lightbox" rel="lightbox" data-bind="attr: { href: '/zoom/'+media[0] }" >
                <img class="thumb" data-bind="attr: { src: '/thumb/'+media[0] }"/>
                <div class="zoomIcon">click</div>
            </a>
        </div>

        <div class="under-thumbnail">
            <div class="product-title accent" data-bind="html: metadata['color']+' - '+metadata['weave']"></div>
            <div data-bind="template: {name: 'attribute/default', data: $parent}"></div>
            <div class="row-fluid">
                <div class="span9">
                    <div class="regular-price" data-bind="text: 'Retail: '+formatCurrency(retail_price)"></div>
                    <div class="sale-price" data-bind="html: 'Steal: '+formatCurrency(sale_price)+'<br/>('+percentOff()+'% off)'"></div>
                </div>
                <div class="span3" style="text-align:right">
                    <label>Quantity</label>
                    <input data-bind="attr: {id: 'quantity'+id()}" class="quantity" value="1" type="text" size="3" style="width:35px;margin-bottom:3px;"/>
                </div>
            </div>
            <div class="row-fluid" style="margin-top:6px">

                <div class="span12" style="text-align:center">
                    <div class="btn btn-warning"
                         data-bind="click: function() {
                                viewModel.cartController.cart.addItem($data, $('#quantity'+id()).val()); },
                                visible:quantity() > 0 && on_hold() != 1 ">
                        Add to Cart <i style="display:none;" class="icon-chevron-right icon-white"></i>
                    </div>
                    <div class="btn btn-inverse btn-mini onhold-mini disabled"
                         style="margin-top:15px;margin-bottom:14px"
                         data-bind="visible: on_hold() == 1 || quantity() <= 0">
                        On Hold<br/><small>Come back soon</small>
                    </div>

                </div>
            </div>
        </div>
    </li>
    <div style="clear:both" data-bind="visible: ($index()+1)%4 == 0"></div>


</script>