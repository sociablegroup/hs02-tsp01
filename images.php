<?php
/* ----------------------------------------------------------------
DYNAMIC IMAGE RESIZING SCRIPT - V2
The following script will take an existing JPG image, and resize it
using set options defined in your .htaccess file (while also providing
a nice clean URL to use when referencing the images)
Images will be cached, to reduce overhead, and will be updated only if
the image is newer than it's cached version.

The original script is from Timothy Crowe's 'veryraw' website, with
caching additions added by Trent Davies:
http://veryraw.com/history/2005/03/image-resizing-with-php/

Further modifications to include antialiasing, sharpening, gif & png 
support, plus folder structues for image paths, added by Mike Harding
http://sneak.co.nz

For instructions on use, head to http://sneak.co.nz
---------------------------------------------------------------- */

// max_width and image variables are sent by htaccess
header("Content-type: image/jpeg");

$image = $_GET["imgfile"];
$thumb_dimensions = list($thumb_width,$thumb_height) = explode("x",$_GET["size"]);
$filename = (strrchr($image, '/')) ? $filename = substr(strrchr($image, '/'), 1) : $image;
$resized_filename = 'cache/'.$thumb_width.'x'.$thumb_height.'-'.$filename;

// first check cache
if(file_exists($resized_filename)){
    $imageModified = @filemtime($image);
    $thumbModified = @filemtime($resized_filename);

    // if thumbnail is newer than image then output cached thumbnail and exit
    if($imageModified<$thumbModified) {
        header("Content-type: image/jpeg");
        header("Last-Modified: ".gmdate("D, d M Y H:i:s",$thumbModified)." GMT");
        readfile($resized_filename);
        exit;
    }
}

// File was not in cache or the image is newer than previously thumbed version of this size so lets make it

$size = getimagesize($image);
$width = $size[0];
$height = $size[1];

// If image is smaller in both width and height just return the image itself
if($width <= $thumb_width && $height <= $thumb_height){
    readfile($image);
    exit;
}

//$width = imagesx($image);
//$height = imagesy($image);

$original_aspect = $width / $height;
$thumb_aspect = $thumb_width / $thumb_height;

if ( $original_aspect <= $thumb_aspect )
{
    // If image is wider than thumbnail (in aspect ratio sense)
    $new_height = $thumb_height;
    $new_width = $width / ($height / $thumb_height);
}
else
{
    // If the thumbnail is wider than the image
    $new_width = $thumb_width;
    $new_height = $height / ($width / $thumb_width);
}

// read image
$ext = strtolower(substr(strrchr($image, '.'), 1)); // get the file extension
switch ($ext) { 
	case 'jpg':     // jpg
		$src = imagecreatefromjpeg($image) or notfound();
		break;
	case 'png':     // png
		$src = imagecreatefrompng($image) or notfound();
		break;
	case 'gif':     // gif
		$src = imagecreatefromgif($image) or notfound();
		break;
	default:
		notfound();
}

$thumb = imagecreatetruecolor( $thumb_width, $thumb_height );
$backgroundColor = imagecolorallocate($thumb, 255, 255, 255);
imagefill($thumb, 0, 0, $backgroundColor);

// Resize and crop
imagecopyresampled ($dst, $src, 0, 0, 0, 0, $tn_width, $tn_height, $width, $height);
$hpos = 0 - ($new_width - $thumb_width) / 2; // Center the image horizontally
$vpos = 0 - ($new_height - $thumb_height) / 2; // Center the image vertically
imagecopyresampled($thumb, $src, $hpos, $vpos, 0, 0, $new_width, $new_height, $width, $height);

// send the header and new image
imagejpeg($thumb, null, -1);
imagejpeg($thumb, $resized_filename, -1); // write the thumbnail to cache as well...

// clear out the resources
imagedestroy($src);
imagedestroy($thumb);

?>