/**
 * Created by JetBrains PhpStorm.
 * User: michael
 * Date: 10/4/11
 * Time: 3:40 PM
 * To change this template use File | Settings | File Templates.
 */
var appsettings =[];
appsettings['authnet_test_mode'] = "FALSE";  // Must be a string of TRUE or FALSE
appsettings['webservices_domain'] = 'http://www.sociableshops.com/'; // must have trailing /
appsettings['partner'] = 'JSD'; // If set to JSD or GMA (must be capital) will show logos and wording for partner. Otherwise shows nothing.
appsettings['wall_code'] = appsettings['partner'] == 'JSD' ? 'today' : appsettings['partner'] == 'GMA' ?  'gma' : '';

var sitedesign =[];
sitedesign['background'] = '#EDEDED';
sitedesign['headerbg'] = '#ededed';



var customer_data = [];
customer_data['company_name'] = 'Home Source International'; // Shows up in authnet checkout page and other palces where company name is required
customer_data['config_id'] = 'hs02-tsp01'; //Set this to the sale id like  cc02-tsp02
customer_data['max_cart_quantity'] = 10;
customer_data['order_offer'] = 'Orders will be shipped in 2-3 weeks.';
customer_data['ship_date_range'] = 'Orders will be shipped in 2-3 weeks.';
customer_data['about_text'] = "";
customer_data['support_email'] = 'info@todayshow-towels.com';
customer_data['site_locked'] = true;
customer_data['sale_ended'] = false;  // Must be true or false. If true the splash code will be disabled and users will not be able to access the store
customer_data['end_timestamp'] = 0; // Set to unix timestamp of datetime when sale should end. Set to 0 to disable.
customer_data['styles'] = [
    {name: "x_header_html_payment_form", value: "<h1>"+customer_data['company_name']+"</h1><h3>WE DO NOT ACCEPT AMERICAN EXPRESS</BR>WE DO NOT SHIP OUTSIDE OF THE CONTINENTAL U.S.</h3><h3>Charges will appear on your credit card statement as <em>Today Show Deal</em></h3>"},
    {name: "x_footer_html_payment_form", value: "<p><strong>NOTICE: PLEASE NOTE THAT ALL SALES ARE FINAL<br/>NO RETURN / NO EXCHANGES</strong><br/><span>"+customer_data['order_offer']+"</span>"},
    {name: "x_footer_email_receipt", value: "<p><strong>NOTICE: PLEASE NOTE THAT ALL SALES ARE FINAL<br/>NO RETURN / NO EXCHANGES</strong><br/><span>"+customer_data['order_offer']+"</span>"},
    {name: "x_footer_html_receipt", value: "<p><strong>NOTICE: PLEASE NOTE THAT ALL SALES ARE FINAL<br/>NO RETURN / NO EXCHANGES</strong><br/><span>"+customer_data['order_offer']+"</span>"}
];

customer_data['start_page'] = customer_data['site_locked'] ? 0: 4;
customer_data['default_template'] = 'pages/splash';
customer_data['enable_inventory_control'] = true; // must be true or false

// Add the state name in lower case as shown in available states array below.
var taxes = [];
taxes['california'] = 9.25;

var availableStates = ['Alabama','Arizona','Arkansas','California','Colorado','Connecticut','Delaware',
    'District of Columbia','Florida','Georgia','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana',
    'Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada',
    'New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon',
    'Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia',
    'Washington','West Virginia','Wisconsin','Wyoming'];
var stateMappings = {Alaska:"AK",Alabama:"AL",Arkansas:"AR",Arizona:"AZ",California:"CA",Colorado:"CO",Connecticut:"CT",
    Delaware:"DE","District of Columbia":"DC",Florida:"FL",Georgia:"GA",Hawaii:"HI",Iowa:"IA",Idaho:"ID",Illinois:"IL",
    Indiana:"IN",Kansas:"KS",Kentucky:"KY",Louisiana:"LA",Massachusetts:"MA",Maryland:"MD",Maine:"ME",Michigan:"MI",
    Minnesota:"MN",Mississippi:"MS",Missouri:"MO",Montana:"MT","North Carolina":"NC","North Dakota":"ND",Nebraska:"NE",
    "New Hampshire":"NH","New Jersey":"NJ","New Mexico":"NM",Nevada:"NV","New York":"NY",Ohio:"OH",Oklahoma:"OK",
    Oregon:"OR",Pennsylvania:"PA","Rhode Island":"RI","South Carolina":"SC","South Dakota":"SD",Tennessee:"TN",Texas:"TX",
    Utah:"UT",Virginia:"VA",Vermont:"VT",Washington:"WA",Wisconsin:"WI","West Virginia":"WV",Wyoming:"WY"};

// ------------------------------------------------
// BRAND DATA
// ------------------------------------------------
var brandOrder = [0]; // Set this to change where each brand appears in order on the page. (It is zero based)
var brands =[
    {
        "id": 0,
        "name": "Home Source International",
        "slug": "home-source",
        "tagline": "",
        "about": "",
        "image": "images/brands/brand1.jpg?cb=1",
        "shipping_cost":function(quantity){
            return 9.95 * quantity;

        }
    }
];


// ------------------------------------------------
// PRODUCT DATA
// ------------------------------------------------
//
//  valid_to_add_callback: function(){ add code here to execute on add to cart. Return false if not good to add or true}
//


// --------------------------------------------------------------------------------------
// Product Set 1
//---------------------------------------------------------------------------------------

//var productSet1Metadata = ['color','size']; // Example would be: productSet2Metadata = ['color','size']; //But color and size must exist in every product in the product source
//var productSet1SizeFilterOptions = ['S','M','L','XL']; // Example options would be:  productSet2SizeFilterOptions = ['XS','S','M','L','XL','XXL','XXXL'];
//var productSet1ColorFilterOptions = ['Black','Blue','Purple']; // Example options would be: productSet2ColorFilterOptions = ['Blue'];

var groupsMetadataKeys = [
    ['color']
];

var productSet1Source =
    [{"id":1,"brand_id":0,"sku":"16250STW01","name":"MicroCotton&#174; Aertex 6-Piece Towel Set","quantity":3350,"retail_price":159,"sale_price":39,"description":"<ul><li>Exceptionally Luxurious, Soft, Plush Super Absorbent, Quick Drying, and Virtually No Linting.</li><li>MicroCotton&#174; is a registered trademark for one of the Finest Cottons in the World and is 100% Natural.</li><li>MicroCotton&#174;: Rated as the Best Towel by Real Simple Magazine.</li><li>Absorbs Water almost Instantaneously, about 2.5 Times Faster than a regular cotton towel.</li><li>Virtually No Linting, so the towel will last many years with normal use and care.</li><li>MicroCotton towels are dyed with High Quality, Azo-Free Reactive Dyes that are Completely Friendly to the Environment.</li><li>100% Cotton</li><li>Bath Towel: 30&#34;x54&#34;, Hand Towel: 16&#34;x30&#34;, Wash Cloth: 13&#34;x13&#34;</li><li>Feel it once. Love it forever.&#8482;</li></ul>","shipping_cost":"","media":"['White.jpg']","group":0,"color":"WHITE"},
        {"id":2,"brand_id":0,"sku":"16250STB91","name":"MicroCotton&#174; Aertex 6-Piece Towel Set","quantity":1000,"retail_price":159,"sale_price":39,"description":"<ul><li>Exceptionally Luxurious, Soft, Plush Super Absorbent, Quick Drying, and Virtually No Linting.</li><li>MicroCotton&#174; is a registered trademark for one of the Finest Cottons in the World and is 100% Natural.</li><li>MicroCotton&#174;: Rated as the Best Towel by Real Simple Magazine.</li><li>Absorbs Water almost Instantaneously, about 2.5 Times Faster than a regular cotton towel.</li><li>Virtually No Linting, so the towel will last many years with normal use and care.</li><li>MicroCotton towels are dyed with High Quality, Azo-Free Reactive Dyes that are Completely Friendly to the Environment.</li><li>100% Cotton</li><li>Bath Towel: 30&#34;x54&#34;, Hand Towel: 16&#34;x30&#34;, Wash Cloth: 13&#34;x13&#34;</li><li>Feel it once. Love it forever.&#8482;</li></ul>","shipping_cost":"","media":"['BlueDusk.jpg']","group":0,"color":"BLUE DUSK"},
        {"id":3,"brand_id":0,"sku":"16250STO10","name":"MicroCotton&#174; Aertex 6-Piece Towel Set","quantity":2300,"retail_price":159,"sale_price":39,"description":"<ul><li>Exceptionally Luxurious, Soft, Plush Super Absorbent, Quick Drying, and Virtually No Linting.</li><li>MicroCotton&#174; is a registered trademark for one of the Finest Cottons in the World and is 100% Natural.</li><li>MicroCotton&#174;: Rated as the Best Towel by Real Simple Magazine.</li><li>Absorbs Water almost Instantaneously, about 2.5 Times Faster than a regular cotton towel.</li><li>Virtually No Linting, so the towel will last many years with normal use and care.</li><li>MicroCotton towels are dyed with High Quality, Azo-Free Reactive Dyes that are Completely Friendly to the Environment.</li><li>100% Cotton</li><li>Bath Towel: 30&#34;x54&#34;, Hand Towel: 16&#34;x30&#34;, Wash Cloth: 13&#34;x13&#34;</li><li>Feel it once. Love it forever.&#8482;</li></ul>","shipping_cost":"","media":"['Linen.jpg']","group":0,"color":"LINEN"},
        {"id":4,"brand_id":0,"sku":"16250STB92","name":"MicroCotton&#174; Aertex 6-Piece Towel Set","quantity":2000,"retail_price":159,"sale_price":39,"description":"<ul><li>Exceptionally Luxurious, Soft, Plush Super Absorbent, Quick Drying, and Virtually No Linting.</li><li>MicroCotton&#174; is a registered trademark for one of the Finest Cottons in the World and is 100% Natural.</li><li>MicroCotton&#174;: Rated as the Best Towel by Real Simple Magazine.</li><li>Absorbs Water almost Instantaneously, about 2.5 Times Faster than a regular cotton towel.</li><li>Virtually No Linting, so the towel will last many years with normal use and care.</li><li>MicroCotton towels are dyed with High Quality, Azo-Free Reactive Dyes that are Completely Friendly to the Environment.</li><li>100% Cotton</li><li>Bath Towel: 30&#34;x54&#34;, Hand Towel: 16&#34;x30&#34;, Wash Cloth: 13&#34;x13&#34;</li><li>Feel it once. Love it forever.&#8482;</li></ul>","shipping_cost":"","media":"['SeaGlass.jpg']","group":0,"color":"SEA GLASS"},
        {"id":5,"brand_id":0,"sku":"16250STN10","name":"MicroCotton&#174; Aertex 6-Piece Towel Set","quantity":3450,"retail_price":159,"sale_price":39,"description":"<ul><li>Exceptionally Luxurious, Soft, Plush Super Absorbent, Quick Drying, and Virtually No Linting.</li><li>MicroCotton&#174; is a registered trademark for one of the Finest Cottons in the World and is 100% Natural.</li><li>MicroCotton&#174;: Rated as the Best Towel by Real Simple Magazine.</li><li>Absorbs Water almost Instantaneously, about 2.5 Times Faster than a regular cotton towel.</li><li>Virtually No Linting, so the towel will last many years with normal use and care.</li><li>MicroCotton towels are dyed with High Quality, Azo-Free Reactive Dyes that are Completely Friendly to the Environment.</li><li>100% Cotton</li><li>Bath Towel: 30&#34;x54&#34;, Hand Towel: 16&#34;x30&#34;, Wash Cloth: 13&#34;x13&#34;</li><li>Feel it once. Love it forever.&#8482;</li></ul>","shipping_cost":"","media":"['Cameo.jpg']","group":0,"color":"CAMEO"},
        {"id":6,"brand_id":0,"sku":"16250STG40","name":"MicroCotton&#174; Aertex 6-Piece Towel Set","quantity":1200,"retail_price":159,"sale_price":39,"description":"<ul><li>Exceptionally Luxurious, Soft, Plush Super Absorbent, Quick Drying, and Virtually No Linting.</li><li>MicroCotton&#174; is a registered trademark for one of the Finest Cottons in the World and is 100% Natural.</li><li>MicroCotton&#174;: Rated as the Best Towel by Real Simple Magazine.</li><li>Absorbs Water almost Instantaneously, about 2.5 Times Faster than a regular cotton towel.</li><li>Virtually No Linting, so the towel will last many years with normal use and care.</li><li>MicroCotton towels are dyed with High Quality, Azo-Free Reactive Dyes that are Completely Friendly to the Environment.</li><li>100% Cotton</li><li>Bath Towel: 30&#34;x54&#34;, Hand Towel: 16&#34;x30&#34;, Wash Cloth: 13&#34;x13&#34;</li><li>Feel it once. Love it forever.&#8482;</li></ul>","shipping_cost":"","media":"['Bamboo.jpg']","group":0,"color":"BAMBOO"},
        {"id":7,"brand_id":0,"sku":"16250STO90","name":"MicroCotton&#174; Aertex 6-Piece Towel Set","quantity":2300,"retail_price":159,"sale_price":39,"description":"<ul><li>Exceptionally Luxurious, Soft, Plush Super Absorbent, Quick Drying, and Virtually No Linting.</li><li>MicroCotton&#174; is a registered trademark for one of the Finest Cottons in the World and is 100% Natural.</li><li>MicroCotton&#174;: Rated as the Best Towel by Real Simple Magazine.</li><li>Absorbs Water almost Instantaneously, about 2.5 Times Faster than a regular cotton towel.</li><li>Virtually No Linting, so the towel will last many years with normal use and care.</li><li>MicroCotton towels are dyed with High Quality, Azo-Free Reactive Dyes that are Completely Friendly to the Environment.</li><li>100% Cotton</li><li>Bath Towel: 30&#34;x54&#34;, Hand Towel: 16&#34;x30&#34;, Wash Cloth: 13&#34;x13&#34;</li><li>Feel it once. Love it forever.&#8482;</li></ul>","shipping_cost":"","media":"['Chocolate.jpg']","group":0,"color":"CHOCOLATE"},
        {"id":8,"brand_id":0,"sku":"16250STK65","name":"MicroCotton&#174; Aertex 6-Piece Towel Set","quantity":3000,"retail_price":159,"sale_price":39,"description":"<ul><li>Exceptionally Luxurious, Soft, Plush Super Absorbent, Quick Drying, and Virtually No Linting.</li><li>MicroCotton&#174; is a registered trademark for one of the Finest Cottons in the World and is 100% Natural.</li><li>MicroCotton&#174;: Rated as the Best Towel by Real Simple Magazine.</li><li>Absorbs Water almost Instantaneously, about 2.5 Times Faster than a regular cotton towel.</li><li>Virtually No Linting, so the towel will last many years with normal use and care.</li><li>MicroCotton towels are dyed with High Quality, Azo-Free Reactive Dyes that are Completely Friendly to the Environment.</li><li>100% Cotton</li><li>Bath Towel: 30&#34;x54&#34;, Hand Towel: 16&#34;x30&#34;, Wash Cloth: 13&#34;x13&#34;</li><li>Feel it once. Love it forever.&#8482;</li></ul>","shipping_cost":"","media":"['Steel.jpg']","group":0,"color":"STEEL"}];