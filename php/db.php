<?php
/**
 * Created by JetBrains PhpStorm.
 * User: michael
 * Date: 5/10/12
 * Time: 7:05 PM
 * To change this template use File | Settings | File Templates.
 */
class db
{
    protected $db_username = 'root';
    protected $db_password = 'jello';
    protected $db_name = 'sociableshops';
    protected $db_uri = 'localhost';

    protected $connection = NULL;   // A database connection object if connected

    /**
     * Expects an object with database connection information. If no object is passed in it will attempt to use
     * default localhost connection settings.
     *
     * Connection object should include the following properties:
     *      username: database username
     *      password: database password
     *      name:   database name
     *      uri:    connection endpoint (ip, url, or localhost)
     *
     * @param null $db_connect
     *
     */
    public function __construct($db_connect=NULL){
        if(!empty($db_connect)){
            $this->db_username = $db_connect->username;
            $this->db_password = $db_connect->password;
            $this->db_name = $db_connect->name;
            $this->db_uri = $db_connect->uri;
        }
    }

    /**
     * Initiate a connection to the database
     *
     * @param null $db_name
     * @return FALSE if failure to connect or connection object if successful
     */
    public function connect($db_name=NULL)
    {
        if($this->connection != NULL) return $this->connection;

        $this->connection = mysql_connect($this->db_uri,$this->db_username,$this->db_password);
        if(!$this->connection){
            echo "Unabled to connect: ".json_encode($this);
            return FALSE;
        }
        mysql_select_db($this->db_name,$this->connection);
        return $this->connection;
    }

    public function query($sql){
        $conn = $this->connect();
        if($conn === FALSE) return FALSE;
        $res = mysql_query($sql);
        $res_array = array();
        while ($row = mysql_fetch_object($res)) {
            array_push($res_array, $row);
        }
        if($res === TRUE and empty($res_array)) return TRUE;
        return $res_array;
    }

    public function close()
    {
        mysql_close($this->connection);
        $this->connection = NULL;
    }
}
