<?php
/**
 * Created by JetBrains PhpStorm.
 * User: michael
 * Date: 5/10/12
 * Time: 7:34 PM
 */
require_once('db.php');

class Salesite
{


    /*
     * Use local database (true) or production database (false)
     */
    protected $test_mode = false;
    protected $use_slave = true;


    public $id = 'hs02-tsp01';
    public $salesite_id = 61;
    protected $db = NULL;
    protected $db_slave = NULL;
    //public $db_test_ip = '166.78.13.115';  // db-master public
    //public $db_prod_ip = '10.181.21.250';  // db-master internal
    //public $db_test_ip = 'localhost';
    //public $db_prod_ip = '50.57.161.121';  // edison
    //public $prod_password = '0o9i8u7y6Y';
    //public $prod_user = 'todayshow';

    // Joyent -- MASTER for Read/Write
    public $db_prod_ip = 'int.cluster.db-prod.joyent.sociablegroup.com';
    public $prod_user = 'sociableshopsite';
    public $prod_password = '0o9i8u7y6Y';

    // Joyent -- SLAVE for READ ONLY
    // Joyent Percona Instance DB1
    public $db_prod_ip_slave = 'int.cluster.db-prod.slave.joyent.sociablegroup.com';
    public $prod_user_slave = 'sshopssite_slave';
    public $prod_password_slave = 'y58lbWw2969K2F6';

    public function __construct()
    {
        if($this->test_mode === TRUE){
            //$this->db = new db();
            $this->db = new db((object)array("username"=>$this->prod_user,"password"=>$this->prod_password,"name"=>"sociableshops-dev","uri"=>$this->db_test_ip));
            $this->db_slave = new db((object)array("username"=>$this->prod_user,"password"=>$this->prod_password,"name"=>"sociableshops-dev","uri"=>$this->db_test_ip));
        }else{
            if($this->use_slave){
                $this->db = new db((object)array("username"=>$this->prod_user,"password"=>$this->prod_password,"name"=>"sociableshops","uri"=>$this->db_prod_ip));
                $this->db_slave = new db((object)array("username"=>$this->prod_user_slave,"password"=>$this->prod_password_slave,"name"=>"sociableshops","uri"=>$this->db_prod_ip_slave));
            }
            else{
                $this->db = new db((object)array("username"=>$this->prod_user,"password"=>$this->prod_password,"name"=>"sociableshops","uri"=>$this->db_prod_ip));
                $this->db_slave = new db((object)array("username"=>$this->prod_user,"password"=>$this->prod_password,"name"=>"sociableshops","uri"=>$this->db_prod_ip));
            }
        }
    }


    /**
     * Used to lock inventory to mark it as unavailable. This is not the same as marking inventory sold. This would be
     * used under a high volume situation for example when a person adds product to their cart we can lock it to ensure
     * it does not get sold between the time the user adds it to their cart and then checks out. Locked inventory will
     * be freed up based on a cron job.
     *
     * You can only lock one sku at a time.
     *
     * @param $lockdata Object with properties: product_id, product_sku, impression_id, quantity
     * @return bool
     */
    public function lock_inventory($lockdata)
    {
        $inventory = $this->inventory_level($lockdata->product_sku);
        if($inventory == false){
            $res_array['success'] = 0;
            $res_array['error_number'] = 1;
            $res_array['error_message'] = 'Inventory returned false.';
            $res_array = array_merge($res_array, (array) $lockdata);
            return $res_array;
        }

        $inventory = $inventory[0];


        if($inventory->free - $lockdata->quantity < 0)
        {
            $res_array['success'] = 0;
            $res_array['error_number'] = 2;
            $res_array['error_message'] = 'Sold out.';
            $res_array = array_merge($res_array, (array) $lockdata);
            return $res_array;
        }

        //$locksql = "INSERT into product_locks VALUES ('',".$lockdata->product_id.",'".$lockdata->product_sku."','".$lockdata->impression_id."',".$lockdata->quantity.",".time().",".time().",'')";
        // Took out prouct-id because tis not used right now and to add items to cart based of metadta or custom id_callbacks it breaks.
        $locksql = "INSERT into product_locks VALUES ('','','".$lockdata->product_sku."','".$lockdata->impression_id."',".$lockdata->quantity.",".time().",".time().",'')";
        $result = $this->db->query($locksql);
        if($result){
            $res_array['success'] = 1;
            $res_array['free'] = $inventory->free - $lockdata->quantity;
            $res_array = array_merge($res_array, (array) $lockdata);
            return $res_array;
        }

        $res_array['success'] = 0;
        $res_array['error_number'] = 3;
        $res_array['error_message'] = 'Lock products query failed.';
        $res_array = array_merge($res_array, (array) $lockdata);
        return $res_array;
    }


    /**
     * $skus should be either array of skus in string format, or one sku in string format, or null for all skus
     *
     *
     * Returns array or false */
    public function inventory_level($skus=null)
    {
        if(is_string($skus)) $skus = "('".$skus."')";
        if(is_array($skus)) $skus = "('".implode("','",$skus)."')";

        $sql = "SELECT product_inventories.sku, available as total, available - COALESCE(sum(orderlineitems.quantity),0) - COALESCE( (SELECT sum(quantity) from product_locks where product_locks.sku = product_inventories.sku),0) as free, COALESCE( (SELECT sum(quantity) from product_locks where product_locks.sku = product_inventories.sku),0) as locks, COALESCE(on_hold,0) as on_hold FROM `product_inventories` LEFT OUTER JOIN orderlineitems on orderlineitems.sku = product_inventories.sku AND orderlineitems.sale_id='".$this->id."' WHERE product_inventories.salesite_id = ".$this->salesite_id."  ".(($skus != null) ? 'AND product_inventories.sku in '.$skus : '')." group by product_inventories.sku";
        $result = $this->db_slave->query($sql);
        return $result==FALSE ? FALSE : $result;
    }

    /**
     *
     * DEPRECATED FOR inventory_level()
     *
     * Kept here for failsafe backup. If current inventory_level() has been working for a while you can delete this function.
     *
     * Returns inventory available for one sku
     * @param $sku
     * @return result
     */
    public function inventory_level_bak($sku)
    {
        //$sql = "SELECT available - (SELECT COALESCE( sum(quantity), 0) from product_locks where sku='".$sku."') - (SELECT COALESCE(sum(quantity),0) from orderlineitems where sku='".$sku."') as free, available as total, sku, on_hold from product_inventories where sku='".$sku."' and salesite_id=".$this->salesite_id;

        $sql = "SELECT available - (SELECT COALESCE( sum(quantity), 0) from product_locks where sku='".$sku."') - (SELECT COALESCE(sum(quantity),0) from orderlineitems inner join responses_authnets on responses_authnets.id = orderlineitems.response_id where sku='".$sku."' and responses_authnets.salesite_id=".$this->salesite_id.") as free, available as total, sku, on_hold from product_inventories where sku='".$sku."' and salesite_id=".$this->salesite_id;
        $result = $this->db_slave->query($sql);
        return $result==FALSE ? FALSE : $result[0];
    }

    public function siteinfo()
    {
        $sql = "SELECT settings from salesites where sale_id ='".$this->id."'";
        $result = $this->db_slave->query($sql);
        return $result==FALSE ? FALSE : unserialize($result[0]->settings);
    }


    public function response($data){
        $this->db->close();
        echo $data;
    }



}
