<?php
/**
 * Created by JetBrains PhpStorm.
 * User: michael
 * Date: 5/10/12
 * Time: 6:56 PM
 * To change this template use File | Settings | File Templates.
 */

require_once('Salesite.php');

$sale_site = new Salesite();
$lock_data = json_decode($_POST['data']);
$lock_result = $sale_site->lock_inventory($lock_data);
$sale_site->response(json_encode($lock_result));

