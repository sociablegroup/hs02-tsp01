<?php
/**
 * Created by JetBrains PhpStorm.
 * User: michael
 * Date: 5/9/12
 * Time: 4:42 PM
 * To change this template use File | Settings | File Templates.
 */

require_once('Salesite.php');

$sale_site = new Salesite();
$skus = json_decode($_GET['skus']);
$result = array();
$result = $sale_site->inventory_level($skus);
$sale_site->response(json_encode($result));