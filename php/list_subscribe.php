<?php
/**
This Example shows how to Subscribe a New Member to a List using the MCAPI.php 
class and do some basic error checking.
 **/
require_once 'MCAPI.class.php';
require_once 'config.inc.php'; //contains apikey

if($_GET['email']){
    $api = new MCAPI($apikey);
    $retval = $api->listSubscribe( $listId, $_GET['email'] );
    if ($api->errorCode){
        echo json_encode(array('success'=>0));
    } else {
        echo json_encode(array('success'=>1));
    }
}
